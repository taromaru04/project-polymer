# Front para Aplicativo Web

Aplicacion de proyecto Banca web

## Instalaciones Previas
Primero se debe tener instalado node version 6.0.0 y bower 1.8.8

## Instalar el Polymer-CLI

Se debe tener instalado el Polymer-CLI en version 1.6.0 . Con `polymer serve` corres el aplicativo de manera local.

## Visualizar tu Aplicacion

```
$ polymer serve
```

## Ruta de inicio

Cuando se habilite la ruta local para visualizar la aplicacion se debe agregar `/inicio` a la ruta para ver la primera pagina del aplicativo. 
